<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Front End 
Route::get('/', function () {
return view('Frontend.konten.index');
});

Route::get('/profile', function () {
    return view('Frontend.konten.profile');
    });

Route::get('/portofolio', function () {
    return view('Frontend.konten.portofolio');
    });


Route::get('/blog', function () {
    return view('Frontend.konten.blog');
});

Route::get('/blog_detail', function () {
    return view('Frontend.konten.blog_detail');
});

Route::get('/contact', function () {
    return view('Frontend.konten.contact');
});


//Backend

Route::get('/admin', function () {
    return view('backend.konten.dashboard.index');
});

Route::get('/login', function () {
    return view('backend.masuk.index');
});
Route::get('/add-konten', function () {
    return view('backend.konten.blog.add-artikel');
});

