<!doctype html>
<html lang="en">
	<head>
		<title>Asuransi Jiwa Kita - Project</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!--meta info-->
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<link rel="icon" type="image/ico" href="frontend/images/fav.ico">
		<!--stylesheet include-->
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/bootstrap.min.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/settings.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/owl.carousel.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/owl.transitions.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/jquery.custom-scrollbar.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/style.css')}}">
		
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/flexslider.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/jackbox.min.css')}}">
		<!--font include-->
		<link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
		<script src="{{asset('frontend/js/modernizr.js')}}"></script>
	</head>
	<body>
		<!--boxed layout-->
		<div class="wide_layout relative w_xs_auto">
			<!--markup header-->
			<header role="banner">
				<!--header top part-->
		        @include('Frontend.komponen.top-menu')
                
                <!--header bottom part-->
			    @include('Frontend.komponen.menu')

            </header>
