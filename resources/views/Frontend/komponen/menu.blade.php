<!--header bottom part-->
<section class="h_bot_part container">
    <div class="clearfix row">
        <div class="col-lg-6 col-md-6 col-sm-4 t_xs_align_c">
            <a href="index.html" class="logo m_xs_bottom_15 d_xs_inline_b">
                <img src="frontend/images/logo.png" alt="">
            </a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8 t_align_r t_xs_align_c">
            <ul class="d_inline_b horizontal_list clearfix t_align_l site_settings">

                <!--language settings-->
                <li class="m_left_5 relative container3d">
                    <a role="button" href="#" class="button_type_2 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none" id="lang_button"><img class="d_inline_middle m_right_10 m_mxs_right_0" src="frontend/images/flag_en.jpg" alt=""><span class="d_mxs_none">English</span></a>
                    <ul class="dropdown_list top_arrow color_light">
                        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="frontend/images/flag_en.jpg" alt="">English</a></li>
                        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="frontend/images/flag_fr.jpg" alt="">French</a></li>
                        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="frontend/images/flag_g.jpg" alt="">German</a></li>
                        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="frontend/images/flag_i.jpg" alt="">Italian</a></li>
                        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="frontend/images/flag_s.jpg" alt="">Spanish</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</section>
<!--main menu container-->
<section class="menu_wrap relative">
    <div class="container clearfix">
        <!--button for responsive menu-->
        <button id="menu_button" class="r_corners centered_db d_none tr_all_hover d_xs_block m_bottom_10">
            <span class="centered_db r_corners"></span>
            <span class="centered_db r_corners"></span>
            <span class="centered_db r_corners"></span>
        </button>
        <!--main menu-->
        <nav role="navigation" class="f_left f_xs_none d_xs_none">	
            <ul class="horizontal_list main_menu clearfix">
                <li class="current relative f_xs_none m_xs_bottom_5"><a href="/" class="tr_delay_hover color_light tt_uppercase"><b>Home</b></a>
                </li>
                <li class="relative f_xs_none m_xs_bottom_5"><a href="#" class="tr_delay_hover color_light tt_uppercase"><b>Profile</b></a>
                </li>
                <li class="relative f_xs_none m_xs_bottom_5"><a href="#" class="tr_delay_hover color_light tt_uppercase"><b>Portfolio</b></a>
                </li>
                <li class="relative f_xs_none m_xs_bottom_5"><a href="#" class="tr_delay_hover color_light tt_uppercase"><b>Blog</b></a>
                </li>
                <li class="relative f_xs_none m_xs_bottom_5"><a href="contact.html" class="tr_delay_hover color_light tt_uppercase"><b>Contact</b></a></li>
            </ul>
        </nav>
        <button class="f_right search_button tr_all_hover f_xs_none d_xs_none">
            <i class="fa fa-search"></i>
        </button>
    </div>
    <!--search form-->
    <div class="searchform_wrap tf_xs_none tr_all_hover">
        <div class="container vc_child h_inherit relative">
            <form role="search" class="d_inline_middle full_width">
                <input type="text" name="search" placeholder="Type text and hit enter" class="f_size_large">
            </form>
            <button class="close_search_form tr_all_hover d_xs_none color_dark">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
</section>
