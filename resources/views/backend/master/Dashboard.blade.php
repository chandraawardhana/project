    @include('backend.master.header')
    @include('backend.master.navbar')

    <main>
        @yield('content')
    </main>

   @include('backend.master.footer')