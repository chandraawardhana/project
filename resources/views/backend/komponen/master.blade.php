@include('backend.komponen.header')

<main>
    @yield('content')
</main>

@include('backend.komponen.footer')